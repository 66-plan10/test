#!/usr/bin/bash
# Copyright (c) 2015-2021 Eric Vidal <eric@obarun.org>
# All rights reserved.
#
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.


export PROG="${0}"
export CLOCK_ENABLED=0
export COLOR_ENABLED=1
export VERBOSITY=1

die() {
    oblog -f "${@}"
    exit 111
}

help(){

    oblog -i "${0} [ --help ] [ --verbosity ]

options:
    --help: display this help.
    --verbosity: increase/decrease verbosity."

    exit 0
}

configure_ssh(){

    oblog -t "Create ~/.ssh/ directory"
    mkdir -p ~/.ssh/ || die "unable to create directory ~/.ssh/"

   # oblog -t "make ~/.ssh/config file"
    #echo "Host git2.obarun.org\n    Port 22023" > ~/.ssh/config || die "unable to create ~/.ssh/config file"

    oblog "Starts ssh-agent"
    eval $(ssh-agent)

    oblog -t "Add private ssh key to ssh agent"
    echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -

    oblog -t "Add ${CI_SERVER_HOST} as known host"
    echo "${SSH_KNOWN_HOSTS}" | base64 -d > ~/.ssh/known_hosts || die "unable to modify file ~/.ssh/known_hosts"

    oblog -t "Chmod 644 ~/.ssh/known_hosts"
    chmod 644 ~/.ssh/known_hosts || die "unable to chmod 644 ~/.ssh/known_hosts"

    oblog "Successfully configured ssh-agent"
}


for arg ; do
  case "$arg" in
    --help) help ;;
    --verbosity=*) VERBOSITY=${arg#*=} ;;
    -*) die "unknown option $arg" ;;
  esac
done

configure_ssh

oblog -t "Configure git with user.name=${GITLAB_USER_LOGIN} and user.email=${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_LOGIN}" || die "unable to configure git with user.name=${GITLAB_USER_LOGIN}"
git config --global user.email "${GITLAB_USER_EMAIL}" || die "unable to configure git with user.email=${GITLAB_USER_EMAIL}"

oblog -t "Cd to /tmp"
cd /tmp || die "unable to cd to /tmp"

oblog "Clone ssh://git@${CI_SERVER_HOST}:22023/${CI_PROJECT_PATH}.git"
git clone "ssh://git@${CI_SERVER_HOST}:22023/${CI_PROJECT_PATH}.git" || die "Unable to clone ssh://git@${CI_SERVER_HOST}:22023/${CI_PROJECT_PATH}.git"

oblog -t "Cd to ${CI_PROJECT_NAME}"
cd "${CI_PROJECT_NAME}"

oblog -t "Checkout to branch ${CI_COMMIT_REF_NAME}"
git checkout "${CI_COMMIT_REF_NAME}" || die "Unable to checkout to ${CI_COMMIT_REF_NAME}"

oblog -t "Retrieve pkgver and pkgrel variables from PKGBUILD"
source trunk/PKGBUILD || die "Unable to source PKGBUILD"

oblog -t "Make directory of version: ${pkgver}-${pkgrel}"
mkdir -p "version/${pkgver}-${pkgrel}" || die "Unable to make directory version/${pkgver}-${pkgrel}"

oblog -t "Copy file from trunk to version/${pkgver}-${pkgrel}"
cp -rf trunk/* "version/${pkgver}-${pkgrel}" || die "Unable to copy trunk/* to version/${pkgver}-${pkgrel}"

oblog "Git add new file"
git add . || die "Unable to git add"

if ! git diff-index --quiet HEAD; then
    oblog -t "Git commit message: upgpkg: ${pkgver}-${pkgrel}"
    git commit -m "upgpkg: ${pkgver}-${pkgrel}" || die "Unable to commit"

    oblog -t "Push the new version"
    git push --all || die "Unable to push at git@${CI_SERVER_HOST}:/${CI_PROJECT_PATH}.git"
fi

oblog "Successfully committed ${CI_PROJECT_NAME}"

exit 0
