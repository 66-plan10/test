# Copyright     : Obarun
#------------------------
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At disroot Dot org>
#----------------
# Obarun PkgSrc : https://git.obarun.org/pkg/obextra/alsa-utils
#---------------------------------------------------------------
# DESCRIPTION ]

pkgname=alsa-utils
pkgver=1.2.7
pkgrel=2
pkgdesc="An alternative implementation of Linux sound support"
url='https://www.alsa-project.org'

track="files/pub/utils"
target="$pkgname-$pkgver"
source=(
    "https://www.alsa-project.org/${track}/${target}.tar.bz2"
)

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'alsa-lib'
    'docbook-xsl'
    'fftw'
    'libsamplerate'
    'ncurses'
    'xmlto'
    'python-docutils'
)

options=(
    'debug'
    'emptydirs'
)

#------------------------
# INSTALL CONFIGURATION ]

depends=(
    'glibc'
    'pciutils'
    'psmisc'
)

optsdepends=(
    'fftw: for alsabat'
)

#----------------
# BUILD PREPARE ]

prepare() {
    cd $pkgname-$pkgver
    autoreconf -vfi
}

#----------------
# BUILD CONTROL ]

_flags=(
    --prefix=/usr
    --sbindir=/usr/bin
    --disable-alsaconf
    --with-udev-rules-dir=/usr/lib/udev/rules.d
)

#--------
# BUILD ]

build() {
    cd $pkgname-$pkgver

    ./configure "${_flags[@]}"
    make
}

check() {
    make -k check -C $pkgname-$pkgver
}

#----------
# PACKAGE ]

package() {
    depends+=(
        'libasound.so'
        'libatopology.so'
        'libformw.so'
        'libmenuw.so'
        'libncursesw.so'
        'libpanelw.so'
        'libsamplerate.so'
)

    make DESTDIR="$pkgdir" install -C $pkgname-$pkgver

    ## dir where to save ALSA state
    install -vdm 755 "$pkgdir/var/lib/alsa/"

    install -vDm 644 $pkgname-$pkgver/README.md -t "$pkgdir/usr/share/doc/$pkgname/"
}

#--------------------
# ARCH LICENSE AUTH ]

arch=('x86_64')
license=('GPL2')

validpgpkeys=(
    'F04DF50737AC1A884C4B3D718380596DA6E59C91' # ALSA Release Team (Pkg Signing Key v1)
                                               # <release@alsa-project.org>
)

sha512sums=('')
