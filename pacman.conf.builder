#
# pacman.conf used if the options --pacman-conf passed at our-build.sh on the .gitlab.yml file
#


[options]
HoldPkg     = pacman glibc
Architecture = auto
IgnorePkg   = systemd
IgnorePkg   = systemd-libs
NoExtract   = usr/lib/libsystemd*.*
Color
CheckSpace
VerbosePkgLists
ParallelDownloads = 5
#XferCommand =

SigLevel    = Required DatabaseOptional
LocalFileSigLevel = Optional

#[obcore-testing]
#SigLevel = Required
#Server = https://repo.obarun.org/obcore-testing/

[obcore]
SigLevel = Required
Server = https://repo.obarun.org/obcore

#[obextra-testing]
#SigLevel = Required
#Server = https://repo.obarun.org/obextra-testing

[obextra]
SigLevel = Required
Server = https://repo.obarun.org/obextra

#[obcommunity-testing]
#SigLevel = Required
#Server = https://repo.obarun.org/obcommunity-testing

[obcommunity]
SigLevel = Required
Server = https://repo.obarun.org/obcommunity

#[obmultilib-testing]
#SigLevel = Required
#Server = https://repo.obarun.org/obmultilib-testing

[obmultilib]
SigLevel = Required
Server = https://repo.obarun.org/obmultilib

#[observice-testing]
#SigLevel = Required
#Server = https://repo.obarun.org/observice-testing

[observice]
SigLevel = Required
Server = https://repo.obarun.org/observice

#[testing]
#Include = /etc/pacman.d/mirrorlist

[core]
Include = /etc/pacman.d/mirrorlist

[extra]
Include = /etc/pacman.d/mirrorlist

#[community-testing]
#Include = /etc/pacman.d/mirrorlist

[community]
Include = /etc/pacman.d/mirrorlist

#[multilib-testing]
#Include = /etc/pacman.d/mirrorlist

#[multilib]
#Include = /etc/pacman.d/mirrorlist
